def creer_annuaire():
    """
    Crée un annuaire vide.
    """
    pass

def ajoute_contact(contact,annuaire):
    """
    Ajoute le contact à la fin de l'annuaire.
    Renvoie l'annuaire modifié.
    """
    pass

def trouve_contact(annuaire,email):
    """
    Cherche un contact identifié par son email dans l'annuaire
    Si aucun n'est trouvé, il faut renvoyer None
    """
    pass
    

## Fonctions relative à un contact

def creer_contact(nom, prenom, email):
    """
    Crée un contact à partir des informations passées en paramètres.
    """
    pass

def prenom(contact):
    """
    Renvoie le prénom d'un contact.
    """
    pass

def nom(contact):
    """
    Renvoie le nom d'un contact.
    """
    pass

def email(contact):
    """
    Renvoie l'email d'un contact.
    """
    pass
